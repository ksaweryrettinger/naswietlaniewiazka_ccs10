/*
 * FreeModbus Libary: TM4C123GH6PM Port
 * Copyright (C) 2019 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>
 *
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

/* ----------------------- Platform includes --------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/include/mbport.h>
#include <FreeMODBUS/mbport/include/port.h>

/* ----------------------- Defines ------------------------------------------*/
/* Timer ticks are counted in multiples of 50us. Therefore 20000 ticks are
 * one second.
 */
#define MB_TIMER_TICKS  (20000L)

/* ----------------------- Static variables ---------------------------------*/
static BOOL bTimerInitialized;
static ULONG ui32TimerLoadValue;

/* ----------------------- Start implementation -----------------------------*/
BOOL xMBPortTimersInit(USHORT usTim1Timerout50us)
{
    if (!bTimerInitialized)
    {
        SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
        TimerConfigure(TIMER0_BASE, TIMER_CFG_ONE_SHOT);
        IntEnable(INT_TIMER0A);
        TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
        bTimerInitialized = TRUE;
    }

    if ((SysCtlClockGet() / MB_TIMER_TICKS) > 0)
    {
        ui32TimerLoadValue = usTim1Timerout50us * (SysCtlClockGet() / MB_TIMER_TICKS);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

inline void vMBPortTimersEnable()
{
    /* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
    TimerLoadSet(TIMER0_BASE, TIMER_BOTH, ui32TimerLoadValue);
    TimerEnable(TIMER0_BASE, TIMER_BOTH);
}

inline void vMBPortTimersDisable()
{
    /* Disable any pending timers. */
    TimerDisable(TIMER0_BASE, TIMER_BOTH);
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
void IntHandlerModbusTimer(void)
{
    ULONG ui32status;
    ui32status = TimerIntStatus(TIMER0_BASE, true);
    TimerIntClear(TIMER0_BASE, ui32status);

    (void) pxMBPortCBTimerExpired();
}

