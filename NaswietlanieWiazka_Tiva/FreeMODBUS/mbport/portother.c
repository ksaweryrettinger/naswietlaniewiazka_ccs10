#include "port.h"
#include "params.h"

/* ----------------------- Other functions -----------------------------*/

void LEDStatus(BOOL bStatus)
{
    if (bStatus) GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5); //switch ON LED
    else GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0); //switch OFF LED
}

void ModbusInterruptDisable(void)
{
    IntDisable(MB_UART_INT);
    IntDisable(INT_WTIMER0A);
}

void ModbusInterruptEnable(void)
{
    IntEnable(MB_UART_INT);
    IntEnable(INT_WTIMER0A);
}
