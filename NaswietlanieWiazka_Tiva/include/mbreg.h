#ifndef MBPORT_INCLUDE_MBREG_H_
#define MBPORT_INCLUDE_MBREG_H_

#include <limits.h>

/* ----------------------- Outline ------------------------------------------*/

/* COILS
 *
 * 1 = Open Valve Z1
 * 2 = Close Valve Z1
 * 3 = Toggle Valve Z2
 * 4 = Toggle Valve Z3
 * 5 = Extend Motor
 * 6 = Retract Motor
 * 7 = Start Bit
 * 8 = Stop Bit
 *
 * INPUT BITS
 *
 * 1-4 = Z1-Z4 Valve Status
 * 5 = Working Status
 * 6 = Motor Status
 * 7 = Motor Screwed Status
 * 8 = Motor Unscrewed Status
 * 9 = System Failure
 * 10 = Local Mode Status (1 = Local Mode)
 *
 * INPUT REGISTERS
 *
 * 1 = Water Temperature (T)
 * 2 = Current (I)
 * 3-16 = Pressure String (P)
 *
 */

/* ----------------------- Register Size ---------------------------------------*/

#define COILS_START               (1)      //coils starting address
#define COILS_NUM                 (8)      //number of coils

#define DISCRETES_START           (10001)  //discretes starting addres
#define DISCRETES_NUM             (12)     //number of discrete inputs

#define INPUT_REGISTERS_START     (30001)  //input registers starting address
#define INPUT_REGISTERS_NUM       (30)     //number of input registers

#define HOLDING_REGISTERS_START   (40001)  //holding registers starting address
#define HOLDING_REGISTERS_NUM     (0)      //number of holding registers

#define REG_NUM_BYTES        (2)      //number of bytes in register
#define REG_ADDR_OFFSET      (1)      //bit/register address offset

/* ----------------------- Addressing ------------------------------------------*/

//COILS
#define COIL_OPEN_Z1         (1)
#define COIL_CLOSE_Z1        (2)
#define COIL_TOGGLE_Z2       (3)
#define COIL_TOGGLE_Z3       (4)
#define COIL_EXTEND_HEAD     (5)
#define COIL_RETRACT_HEAD    (6)
#define COIL_START           (7)
#define COIL_STOP            (8)

//DISCRETE INPUTS
#define DISCRETE_Z1              (1)
#define DISCRETE_Z2              (2)
#define DISCRETE_Z3              (3)
#define DISCRETE_Z4              (4)
#define DISCRETE_WORKING         (5)
#define DISCRETE_HEAD_RETRACTED  (6)
#define DISCRETE_SCREWED         (7)
#define DISCRETE_UNSCREWED       (8)
#define DISCRETE_ALERT           (9)
#define DISCRETE_LOCAL_MODE      (10)
#define DISCRETE_HEAD_EXTENDED   (11)
#define DISCRETE_TPG362_ON       (12)

//INPUT REGISTERS
#define INPUT_TEMPERATURE    (1)
#define INPUT_CURRENT        (2)
#define INPUT_PRESSURE_START (3)

/* ----------------------- Global variables ----------------------------------*/

UCHAR  ucMBCoils[(COILS_NUM + (CHAR_BIT - 1)) / CHAR_BIT];            //Coils
UCHAR  ucMBDiscretes[(DISCRETES_NUM + (CHAR_BIT - 1)) / CHAR_BIT];    //Discrete Inputs
USHORT usMBInputReg[INPUT_REGISTERS_NUM];                             //Input Registers
USHORT usMBHoldingReg[HOLDING_REGISTERS_NUM];                         //Holding Registers

BOOL bMBCoilIsWritten(UCHAR ucIndex);
BOOL bMBHoldingRegisterIsWritten(UCHAR ucIndex);

#endif /* MBPORT_INCLUDE_MBREG_H_ */
