#ifndef INCLUDE_PARAMS_H_
#define INCLUDE_PARAMS_H_

/* ----------------------- Modbus Configuration -------------------------------*/

#define MB_STELLARIS_DEBUG      (0)
#define MB_MODE                 (MB_RTU)
#define MB_SLAVEID              (0x01)
#define MB_PORT                 (0)
#define MB_BAUDRATE             (19200)
#define MB_PARITY               (MB_PAR_EVEN)

#if (MB_STELLARIS_DEBUG)

   #define MB_UART_MODULE       (UART0_BASE)
   #define MB_UART_INT          (INT_UART0)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART0)
   #define MB_UART_GPIO_PORT    (GPIO_PORTA_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_0)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_1)
   #define MB_UART_GPIO_RX      (GPIO_PA0_U0RX)
   #define MB_UART_GPIO_TX      (GPIO_PA1_U0TX)

#else

   #define MB_UART_MODULE       (UART3_BASE)
   #define MB_UART_INT          (INT_UART3)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART3)
   #define MB_UART_GPIO_PORT    (GPIO_PORTC_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_6)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_7)
   #define MB_UART_GPIO_RX      (GPIO_PC6_U3RX)
   #define MB_UART_GPIO_TX      (GPIO_PC7_U3TX)

#endif

/*------------------------ Other parameters -----------------------------------*/

#define PRESSURE_BUFFER_SIZE        (32)
#define PRESSURE_STRING_LENGTH      (27)
#define PRESSURE_SAFETY_HIGH        (0.0051f)
#define PRESSURE_SAFETY_LOW         (0.0049f)
#define TEMP_AVERAGE_SAMPLES        (1000)

/*------------------------ GPIO Pin Configuration -----------------------------*/

/* PB2 - I2C SCL (I/O Extender)
 * PB3 - I2C SDA (I/O Extender)
 *
 * PC4 - UART Rx (Pressure Meter)
 * PC5 - UART Tx (Pressure Meter)
 * PC6 - UART Rx (Modbus)
 * PC7 - UART Tx (Modbus)
 * PD6 - UART Rx (OPTIONAL)
 * PD7 - UART Tx (OPTIONAL)
 *
 * PB4 - GPIO Input (High Current, OPTIONAL)
 * PA5 - GPIO Input (High Current, OPTIONAL)
 * PA6 - GPIO Input (High Current, OPTIONAL)
 * PA7 - GPIO Input (High Current, OPTIONAL)
 * PE2 - GPIO Input (OPTIONAL)
 *
 * PB7 - GPIO Output (Pressure Meter Data Direction)
 *
 * PD0 - Analogue Input Amplified (Temperature)
 * PD1 - Analogue Input Amplified (Current)
 * PD2 - Analogue Input Amplified (OPTIONAL)
 * PD3 - Analogue Input (OPTIONAL)
 * PE1 - Analogue Input (OPTIONAL)
 *
 */

#endif
