// Naświetlanie Wiązką Wewnętrzną - TI TM4C123GH6PM
// Copyright (C) 2020 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "tm4c123gh6pm.h"
#include "params.h"
#include "mbreg.h"

/* ----------------------- Defines -----------------------------------------------*/

#define ADDRESS_I2C_1             (0b0100000) //read
#define ADDRESS_I2C_2             (0b0100001) //read
#define ADDRESS_I2C_3             (0b0100100) //write
#define ADDRESS_I2C_4             (0b0100000) //write
#define ADDRESS_I2C_5             (0b0100001) //write
#define I2C_SPEED                 (100000)
#define I2C_MASTER_TIMEOUT        (30) //I2C Clocks

/* ----------------------- Function prototypes ------------------------------------*/

void xSequenceADC(ULONG*);
void bPressureDataRead(USHORT*);
BOOL bWriteI2C(UCHAR, UCHAR, ULONG);
UCHAR ucReadI2C(UCHAR, ULONG);
void xI2CMasterWait(ULONG*, ULONG);

/* ----------------------- Static variables ------------------------------------*/

static UCHAR i = 0;
static ULONG ulTimerI2CLoadValue = 0;
static CHAR cByte = 0;
static CHAR cBytePrev = 0;
static CHAR cRcvBufferPressure[PRESSURE_BUFFER_SIZE] = { 0 };
static CHAR cP2[12];
static BOOL bLastPressureLow = FALSE;
static double P2;
static volatile UCHAR ucPressureRcvBufferPos = 0;
static volatile BOOL bI2CTimeout = FALSE;
static volatile BOOL bTPG362Timeout = FALSE;
static volatile BOOL bPressureDataReady = FALSE;

int main(void)
{
    /*------------------------ Variables ------------------------------------------------*/

    UCHAR ucStatePLC = 0; //monitors state of all PLC outputs
    UCHAR ucDataI2C1 = 0;
    UCHAR ucDataI2C2 = 0;
    UCHAR ucMBDataI2C1 = 0;
    UCHAR ucMBDataI2C2 = 0;
    ULONG ulDataADC[2] = { 0 };
    USHORT usPressure = 0;
    USHORT usTempAverageCount = 0;
    ULONG ulTempSum = 0;

    /*------------------------ FPU Module -----------------------------------------------*/

    FPULazyStackingEnable();
    FPUEnable();

    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------ GPIO Pin Configuration -----------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    //External LED (PB5)
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5);

    //UART (Pressure Meter, RS-485)
    GPIOPinConfigure(GPIO_PC4_U4RX);
    GPIOPinConfigure(GPIO_PC5_U4TX);
    GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);

    //UART (Modbus, RS-232)
    GPIOPinConfigure(MB_UART_GPIO_RX);
    GPIOPinConfigure(MB_UART_GPIO_TX);
    GPIOPinTypeUART(MB_UART_GPIO_PORT, MB_UART_GPIO_PIN_RX | MB_UART_GPIO_PIN_TX);

    //I2C (I/O Expander 1-3)
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    //I2C2 (I/O Expander 4-5)
    GPIOPinConfigure(GPIO_PA6_I2C1SCL);
    GPIOPinConfigure(GPIO_PA7_I2C1SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTA_BASE, GPIO_PIN_6);
    GPIOPinTypeI2C(GPIO_PORTA_BASE, GPIO_PIN_7);

    //GPIO Output (Pressure Meter Data Direction)
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_7);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_7, 0); //default mode (read)

    /*------------------------ ADC configuration (PD1-PD2) ------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_2);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH7);                             //Sample PD0 (Temperature)
    ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH5 | ADC_CTL_IE | ADC_CTL_END);  //Sample PD2 (Current)
    ADCHardwareOversampleConfigure(ADC0_BASE, 64);                                      //64-Sample Averaging
    ADCSequenceEnable(ADC0_BASE, 1);

    /*------------------------------ UART Configuration (Pressure Meter) ----------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART4);
    UARTConfigSetExpClk(UART4_BASE, SysCtlClockGet(), 9600, (UART_CONFIG_WLEN_8 |
            UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
    UARTFIFODisable(UART4_BASE);
    IntEnable(INT_UART4);
    UARTIntEnable(UART4_BASE, UART_INT_RX);

    /*------------------------------ I2C Configuration ----------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
     SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);
     I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), FALSE); //standard speed
     I2CMasterInitExpClk(I2C1_BASE, SysCtlClockGet(), FALSE); //standard speed

     /*------------------------------ I2C Timeout Timer ---------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
     TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
     IntEnable(INT_TIMER1A);
     TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
     ulTimerI2CLoadValue = (SysCtlClockGet() / I2C_SPEED) * I2C_MASTER_TIMEOUT;

     /*------------------------------ Expander Initialization ---------------------------*/

     (void) bWriteI2C(0, ADDRESS_I2C_4, I2C1_BASE); //initialize all outputs to 0 on U4 expander
     (void) bWriteI2C(0, ADDRESS_I2C_5, I2C1_BASE); //initialize U5 expander to 0 (low vacuum)

     /*------------------------------ TPG362 UART Timer ---------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
     TimerConfigure(TIMER2_BASE, TIMER_CFG_ONE_SHOT);
     IntEnable(INT_TIMER2A);
     TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

    /*------------------------ MODBUS Protocol Stack Initialization ---------------------*/

    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    (void) eMBEnable();

    /*------------------------ MODBUS Bit/Register Initialization -----------------------*/

    //Read GPIO input states
    ucDataI2C1 = ucReadI2C(ADDRESS_I2C_1, I2C0_BASE);
    ucDataI2C2 = ucReadI2C(ADDRESS_I2C_2, I2C0_BASE);

    //Input pins modified - these changes are introduced to avoid changing the linux application
    ucDataI2C1 &= 0x0F; //clear 4 MSBs in U1
    ucDataI2C1 |= ucDataI2C2 & 0xF0; //copy 4 MSBs from U2 to U1
    ucDataI2C2 &= 0x0F; //clear 4 MSBs in U2

    //Flip U1 expander bits
    ucMBDataI2C1 = ucDataI2C1;
    ucMBDataI2C1 ^= 1 << 5; //head retracted status

    //Flip U2 expander bits
    ucMBDataI2C2 = ucDataI2C2;
    ucMBDataI2C2 ^= 1 << 2; //head extended status
    ucMBDataI2C2 ^= 1 << 1; //local mode

    //Store GPIO inputs as Modbus discrete inputs
    xMBUtilSetBits(ucMBDiscretes, 0, 8, ucMBDataI2C1);
    xMBUtilSetBits(ucMBDiscretes, 8, 3, ucMBDataI2C2 & 0x07);

    //Read temperature and current data
    xSequenceADC(ulDataADC);

    //Store temperature reading for averaging
    ulTempSum += (USHORT) ulDataADC[0];
    usTempAverageCount++;

    //Store current data
    usMBInputReg[INPUT_CURRENT - 1] = (USHORT) ulDataADC[1];

    //Read pressure data and store in input registers
    bPressureDataRead(&usPressure);

    while (1)
    {
        /*--------------------------------- MODBUS ------------------------------------------*/

        (void) eMBPoll();

        /*--------------------------------- Read GPIO Inputs (I2C) --------------------------*/

        //Read GPIO input pins
        ucDataI2C1 = ucReadI2C(ADDRESS_I2C_1, I2C0_BASE);
        ucDataI2C2 = ucReadI2C(ADDRESS_I2C_2, I2C0_BASE);

        //Input pins modified - these changes are introduced to avoid changing the linux application
        ucDataI2C1 &= 0x0F; //clear 4 MSBs in U1
        ucDataI2C1 |= ucDataI2C2 & 0xF0; //copy 4 MSBs from U2 to U1
        ucDataI2C2 &= 0x0F; //clear 4 MSBs in U2

        //Flip U1 expander bits
        ucMBDataI2C1 = ucDataI2C1;
        ucMBDataI2C1 ^= 1 << 5; //head retracted status

        //Flip U2 expander bits
        ucMBDataI2C2 = ucDataI2C2;
        ucMBDataI2C2 ^= 1 << 2; //head extended status
        ucMBDataI2C2 ^= 1 << 1; //local mode

        //Store GPIO inputs as Modbus discrete inputs
        xMBUtilSetBits(ucMBDiscretes, 0, 8, ucMBDataI2C1);
        xMBUtilSetBits(ucMBDiscretes, 8, 3, ucMBDataI2C2 & 0x07);

        /*--------------------------------- Read Analogue Inputs (ADC) ----------------------*/

        //Read temperature and current data
        xSequenceADC(ulDataADC);
        ulTempSum += (USHORT) ulDataADC[0];
        usTempAverageCount++;

        //Store temperature data
        if (usTempAverageCount == TEMP_AVERAGE_SAMPLES)
        {
            usMBInputReg[INPUT_TEMPERATURE - 1] = (USHORT) (ulTempSum/TEMP_AVERAGE_SAMPLES);
            usTempAverageCount = 0;
            ulTempSum = 0;
        }

        //Store current data
        usMBInputReg[INPUT_CURRENT - 1] = (USHORT) ulDataADC[1];

        //Read pressure data and store in input registers
        bPressureDataRead(&usPressure);

        /*--------------------------------- Coil Safety Checks ------------------------------*/

        if (bMBCoilIsWritten(COIL_EXTEND_HEAD) && xMBUtilGetBits(ucMBCoils, COIL_EXTEND_HEAD - 1, 1))
        {
            //DO NOT extend Head if Z1 valve is closed
            if (xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z1 - 1, 1))
            {
                //Reset coil status
                xMBUtilSetBits(ucMBCoils, COIL_EXTEND_HEAD - 1, 1, 0);
            }
        }
        else if (bMBCoilIsWritten(COIL_RETRACT_HEAD) && xMBUtilGetBits(ucMBCoils, COIL_RETRACT_HEAD - 1, 1))
        {
            //DO NOT retract Head if Z1 valve is closed
            if (xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z1 - 1, 1))
            {
                //Reset coil status
                xMBUtilSetBits(ucMBCoils, COIL_RETRACT_HEAD - 1, 1, 0);
            }
        }
        else if (bMBCoilIsWritten(COIL_OPEN_Z1) && xMBUtilGetBits(ucMBCoils, COIL_OPEN_Z1 - 1, 1))
        {
            //DO NOT open Z1 if another valve is open OR pressure is too high (system alert)
            if ((xMBUtilGetBits(ucMBDiscretes, 1, 3) != 0x07) || xMBUtilGetBits(ucMBDiscretes, DISCRETE_ALERT - 1, 1))
            {
               //Reset coil status
               xMBUtilSetBits(ucMBCoils, COIL_OPEN_Z1 - 1, 1, 0);
            }
        }
        else if (bMBCoilIsWritten(COIL_CLOSE_Z1) && xMBUtilGetBits(ucMBCoils, COIL_CLOSE_Z1 - 1, 1))
        {
            //DO NOT close Z1 if Head is extended
            if (xMBUtilGetBits(ucMBDiscretes, DISCRETE_HEAD_EXTENDED - 1, 1))
            {
                //Reset coil status
                xMBUtilSetBits(ucMBCoils, COIL_CLOSE_Z1 - 1, 1, 0);
            }
        }
        else if (bMBCoilIsWritten(COIL_TOGGLE_Z2) && xMBUtilGetBits(ucMBCoils, COIL_TOGGLE_Z2 - 1, 1))
        {
            //DO NOT open Z2 if Z1, Z3 or Z4 are open
            if (xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z2 - 1, 1) && (xMBUtilGetBits(ucMBDiscretes, 2, 2) != 0x03) && (xMBUtilGetBits(ucMBDiscretes, 0, 1) != 0x01))
            {
                //Reset coil status
                xMBUtilSetBits(ucMBCoils, COIL_TOGGLE_Z2 - 1, 1, 0);
            }
        }
        else if (bMBCoilIsWritten(COIL_TOGGLE_Z3) && xMBUtilGetBits(ucMBCoils, COIL_TOGGLE_Z3 - 1, 1))
        {
            //DO NOT open Z3 if Z1, Z2 or Z4 are open
            if (xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z3 - 1, 1) && (xMBUtilGetBits(ucMBDiscretes, 0, 2) != 0x03) && (xMBUtilGetBits(ucMBDiscretes, 3, 1) != 0x01))
            {
                //Reset coil status
                xMBUtilSetBits(ucMBCoils, COIL_TOGGLE_Z3 - 1, 1, 0);
            }
        }

        /*--------------------------------- Write to PLC ------------------------------------*/

        if (ucStatePLC != xMBUtilGetBits(ucMBCoils, 0, 8)) //ensure outputs always match coils
        {
            ucStatePLC = xMBUtilGetBits(ucMBCoils, 0, 8);
            (void) bWriteI2C(ucStatePLC, ADDRESS_I2C_4, I2C1_BASE);
        }

        /*--------------------------------- Clear Coils -------------------------------------*/

        //Open Z1 (clear when opened)
        if ((ucMBCoils[0] & 0x01) != 0 && !xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z1 - 1, 1))
        {
            ucMBCoils[0] &= ~(0x01);
        }

        //Close Z1 (clear when closed)
        if ((ucMBCoils[0] & (1 << 1)) != 0 && xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z1 - 1, 1))
        {
            ucMBCoils[0] &= ~(1 << 1);
        }

        //Extend Head (clear when extended)
        if ((ucMBCoils[0] & (1 << 4)) != 0 && xMBUtilGetBits(ucMBDiscretes, DISCRETE_HEAD_EXTENDED - 1, 1))
        {
            ucMBCoils[0] &= ~(1 << 4);
        }

        //Retract Head (clear when retracted)
        if ((ucMBCoils[0] & (1 << 5)) != 0 && xMBUtilGetBits(ucMBDiscretes, DISCRETE_HEAD_RETRACTED - 1, 1))
        {
            ucMBCoils[0] &= ~(1 << 5);
        }

        //START signal (clear on STOP)
        if ((ucMBCoils[0] & (1 << 6)) != 0 && (ucMBCoils[0] & (1 << 7)) != 0)
        {
            ucMBCoils[0] &= ~(1 << 6);
        }

        //STOP signal (clear on Z3 Open)
        if ((ucMBCoils[0] & (1 << 7)) != 0 && !xMBUtilGetBits(ucMBDiscretes, DISCRETE_Z3 - 1, 1))
        {
            ucMBCoils[0] &= ~(1 << 7);
        }
    }
}

void bPressureDataRead(USHORT* usPressure)
{
    //Read pressure meter data
    if (bPressureDataReady)
    {
        //Disable interrupts
        IntDisable(INT_UART4);

        //Check message length
        if (ucPressureRcvBufferPos == (PRESSURE_STRING_LENGTH + 2))
        {
            //Check string
            if (cRcvBufferPressure[9] == 'E' && cRcvBufferPressure[23] == 'E' )
            {
                //Convert P2 reading
                memcpy(cP2, &cRcvBufferPressure[16], 11);
                cP2[11] = '\0';
                P2 = atof(cP2);

                //Write pressure status to GPIO output (PLC)
                if (P2 >= PRESSURE_SAFETY_HIGH && bLastPressureLow) //LOW->HIGH PRESSURE (BAD VACUUM)
                {
                    bLastPressureLow = FALSE;
                    (void) bWriteI2C(0, ADDRESS_I2C_5, I2C1_BASE); //set P0 bit on U5 expander
                }
                else if (P2 <= PRESSURE_SAFETY_LOW && !bLastPressureLow) //HIGH->LOW PRESSURE (GOOD VACUUM)
                {
                    bLastPressureLow = TRUE;
                    (void) bWriteI2C(1, ADDRESS_I2C_5, I2C1_BASE); //clear P0 bit on U5 expander
                }

                //Store pressure strings in Modbus input registers
                for (i = 0; i < INPUT_REGISTERS_NUM - 3; i++)
                {
                    usMBInputReg[i + INPUT_PRESSURE_START - 1] = cRcvBufferPressure[i];
                }

                //Restart timer
                bTPG362Timeout = FALSE;
                TimerLoadSet(TIMER2_BASE, TIMER_BOTH, SysCtlClockGet() * 5);
                TimerEnable(TIMER2_BASE, TIMER_BOTH);

                //Set 'TPG362 Active' bit
                xMBUtilSetBits(ucMBDiscretes, DISCRETE_TPG362_ON - 1, 1, 1);
            }
        }

        //Reset ISR counters and flags
        ucPressureRcvBufferPos = 0;
        bPressureDataReady = FALSE;

        //Re-enable interrupts
        IntEnable(INT_UART4);
    }

    //Check for TPG362 connection problems
    if (bTPG362Timeout)
    {
        xMBUtilSetBits(ucMBDiscretes, DISCRETE_TPG362_ON - 1, 1, 0);
        bTPG362Timeout = FALSE;
    }
}

void IntHandlerPressureUART(void)
{
    ULONG ui32status;
    ui32status = UARTIntStatus(UART4_BASE, true);
    UARTIntClear(UART4_BASE, ui32status);

    //Receive interrupt
   if ((ui32status & UART_INT_RX) != 0u)
   {
       //Always fetch new byte
       cByte = (CHAR) UARTCharGet(UART4_BASE);

       //Store byte in buffer
       if (ucPressureRcvBufferPos < PRESSURE_BUFFER_SIZE)
       {
           cRcvBufferPressure[ucPressureRcvBufferPos++] = cByte;
       }

       //Check for end of message (<CR><LF>)
       if (cBytePrev == 0x0D && cByte == 0x0A)
       {
           bPressureDataReady = TRUE;
           cByte = 0;
       }

       cBytePrev = cByte;
   }
}

void IntHandlerTimerTPG362(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER2_BASE, TRUE);
    TimerIntClear(TIMER2_BASE, ui32status);
    bTPG362Timeout = TRUE;
}

BOOL bWriteI2C(UCHAR ucData, UCHAR i2cAddress, ULONG ulI2C_BASE)
{
    ULONG ulErrorI2C = I2C_MASTER_ERR_NONE;

    //I2C send mode
    I2CMasterSlaveAddrSet(ulI2C_BASE, i2cAddress, FALSE);

    //Send data byte
    I2CMasterDataPut(ulI2C_BASE, ucData);
    I2CMasterControl(ulI2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    xI2CMasterWait(&ulErrorI2C, ulI2C_BASE);

    //Check for errors
    if (ulErrorI2C == I2C_MASTER_ERR_NONE) return TRUE;
    else return FALSE;
}

UCHAR ucReadI2C(UCHAR i2cAddress, ULONG ulI2C_BASE)
{
    ULONG ulErrorI2C = I2C_MASTER_ERR_NONE;
    ULONG ulData = 0;

    //I2C Receive Mode
    I2CMasterSlaveAddrSet(ulI2C_BASE, i2cAddress, TRUE);
    I2CMasterControl(ulI2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
    xI2CMasterWait(&ulErrorI2C, ulI2C_BASE);

    //Read data byte
    if (ulErrorI2C == I2C_MASTER_ERR_NONE) ulData = I2CMasterDataGet(ulI2C_BASE);
    return (UCHAR) ulData;
}

void xI2CMasterWait(ULONG * pulErrorI2C, ULONG ulI2C_BASE)
{
    //Start I2C timeout timer
    bI2CTimeout = FALSE;
    TimerLoadSet(TIMER1_BASE, TIMER_BOTH, ulTimerI2CLoadValue);
    TimerEnable(TIMER1_BASE, TIMER_BOTH);

    //Wait for response
    while (I2CMasterBusy(ulI2C_BASE))
    {
        if (bI2CTimeout) //check for timeout
        {
            //Timeout occured
            *pulErrorI2C = I2C_MASTER_ERR_CLK_TOUT;
            return;
        }
    }

    TimerDisable(TIMER1_BASE, TIMER_BOTH); //response received, switch off timer

    //Check for other errors
    *pulErrorI2C = I2CMasterErr(ulI2C_BASE);
    return;
}

void IntHandlerTimerI2C(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER1_BASE, TRUE);
    TimerIntClear(TIMER1_BASE, ui32status);
    bI2CTimeout = TRUE;
}

void xSequenceADC(ULONG *ulDataADC)
{
    ADCIntClear(ADC0_BASE, 1);
    ADCProcessorTrigger(ADC0_BASE, 1);
    while(!ADCIntStatus(ADC0_BASE, 1, false)) {}
    ADCSequenceDataGet(ADC0_BASE, 1, ulDataADC);
    if (ulDataADC[1] <= 3) ulDataADC[1] = 0;
}
